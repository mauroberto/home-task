/**
 * 
 */
$(document).ready(function(){
	var addGroup = function(group){
		
		var newGroup = "<li> <a href='#' id='group"+group.id+"'> <span>"+group.name+"</span>" +
		"<i title='Editar' id='editGroup"+group.id+"' class='fa fa-edit btn edit' data-toggle='modal' data-target='#editGroupModal"+group.id+"'></i>" +
		"<i title='Remover' id='removeGroup"+group.id+"' class='fa fa-trash-o btn remove'> </i> </a> </li>";
		
		$("#groupList").append(newGroup);
		
		$("#editGroup"+group.id).click(function(){
			var newModalEditGroup = 
			'<div class="modal fade" id="editGroupModal'+group.id+'" tabindex="-1" role="dialog"'+
				'aria-labelledby="addGroupModal" aria-hidden="true">'+
				'<div class="modal-dialog">'+
					'<div class="modal-content">'+
						'<div class="modal-header">'+
							'<button type="button" class="close" data-dismiss="modal"'+
								'aria-hidden="true">&times;</button>'+
							'<h4 class="modal-title" id="addTaskMotalLabel">Adicionar Grupo:</h4>'+
						'</div>'+
						'<form id="editGroup" class="form-horizontal" action="addGroup" method="post">'+
							'<div class="modal-body">'+
								'<div class="form-group">'+
									'<label class="col-sm-3 col-sm-3 control-label">Nome do'+
										' Grupo:</label>'+
									'<div class="col-sm-9">'+
										'<input type="text" class="form-control" id="nameEdit" name="name"'+
											'value="'+group.name+'">'+
										'<div class="display-none has-error" id="error-message">Nome inválido!</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="modal-footer">'+
								'<button type="button" class="btn btn-default" data-dismiss="modal" id="close">Fechar</button>'+
								'<button type="submit" id="addGroupBtn" class="btn btn-ht">Salvar</button>'+
							'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
			'</div>';
			
			$("#editGroupModals").append(newModalEditGroup);
			
			$("#editGroup", "#editGroupModal"+group.id).submit(function(){
				var name = $("#nameEdit", this).val();
				if(name.trim().length > 0 && name != group.name){
					$.ajax({
						url:"../editGroup",
						type:"post",
						dataType:"json",
						data:{name:name, idGroup:group.id},
						success:function(response){
							if(response == "error"){
								$("#error-message",'#editGroupModal'+group.id).show();
							}else{
								group.name = response.name;
								$("#close","#editGroupModal"+group.id).click();
								$("span","#group"+group.id).html(group.name);
							}
						}
					});
				}else{
					$("#error-message",'#editGroupModal'+group.id).show();
				}
				return false;
			});
		});
		
		
		
		$("#removeGroup"+group.id).click(function(){
			
			
			bootbox.confirm("Deseja realmente excluir esse grupo? Todos os dados relacionados a ele serão perdidos.",function(result){
				
				if(result){
					$.ajax({
						url:"../removeGroup",
						data:{idGroup:group.id},
						type:"post",
						success:function(response){
							console.log(response);
							if(response == "true"){
								$("#group"+group.id).closest("li").remove();
								$("#editGroupModal"+group.id).remove();
								if(idGroup == group.id){
									loadGeneralSession();
								}
							}else{
								
							}
						}
					});
				}
			
			});
		});
		
		$("#group"+group.id).click(function(){
			$("#membersList").html("<h3>MEMBROS<i title='Adicionar membro ao grupo' data-toggle='modal' data-target='#addGroupMember' class='fa fa-plus addMembros btn fa-3x'></i></h3>");
			$("#groupName").html("<b>HomeTask</b> | "+group.name);
			$("title").html("Hometask - "+group.name);
			loadMembers(group.id);
			loadTasks(group.id);
			idGroup = group.id;
			$("* #idGroup").val(group.id);
		});
		
		$("#addGrupo").modal('hide');
		$("#name").val("");
	}
	
	$.ajax({
		url:"../listGroups",
		type:"get",
		dataType:"json",
		success:function(groups){
			if(groups != "error"){
				for(var i=0; i<groups.length; i++){
					addGroup(groups[i]);
				}
			}
		},
		error:function(){
			
		}
	});
	
	$("#addGroupForm").submit(function(){
		var name = $("#name").val();
		
		if(name.trim().length > 0){
			$.ajax({
				url:"../addGroup",
				type:"post",
				dataType:"json",
				data:{name:name},
				success:function(response){
					if(response == "error"){
						$("#error-message", "#addGroupForm").show();
					}else{
						addGroup(response);
					}
				}
			});
		}else{
			$("#error-message", "#addGroupForm").show();
		}
		return false;
	});
	
	$("#general").click(function(){
		$("#addTaskBtn").fadeOut();
		membersOnTask = [];
		
		$("#groupName").html("<b>HomeTask</b> | GERAL");
		$("title").html("Hometask - Geral");
		$("#taskGridster").html("");
		$("#editTaskModals").html("");
		$("#descriptionsZoom").html("");
		$("#membersList").fadeOut();
		idGroup = null;
		
	});
});