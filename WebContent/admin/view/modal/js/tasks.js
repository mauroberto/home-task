/**
 * 
 */

var isValidatedName = false;
var isvalidatedDescription = false;
var isValidatedTime = false;
var isValidatedDate = false;

var isValidatedNameEdit = [];
var isValidatedDescriptionEdit = [];
var isValidatedTimeEdit = [];
var isValidatedDateEdit = [];

var validateName = function(name, formGroup, errorMessage){
	if(name != null && name.trim().length > 0){
		$(formGroup).addClass("has-success").removeClass("has-error");
		$(errorMessage).html("");
		return true;
	}else{
		$(formGroup).addClass("has-error").removeClass("has-success");
		$(errorMessage).html("O nome não pode ser vazio!");
		return false;
	}
}

var validateDescription = function(description, formGroup, errorMessage){
	if(description != null && description.trim().length > 0){
		$(formGroup).addClass("has-success").removeClass("has-error");
		$(errorMessage).html("");
		return true;
	}else{
		$(formGroup).addClass("has-error").removeClass("has-success");
		$(errorMessage).html("A descrição não pode ser vazia!");
		return false;
	}
}

var validateTime = function(time, formGroup, errorMessage){
	var regex = /^([01]\d|2[0-3]):?([0-5]\d)$/;
	if(time.trim().length > 0 && regex.test(time)){
		$(formGroup).addClass("has-success").removeClass("has-error");
		$(errorMessage).html("");
		return true;
	}else{
		$(formGroup).addClass("has-error").removeClass("has-success");
		$(errorMessage).html("O horário é obrigatório, deve ser válido e estar no formato HH:MM");
		return false;
	}
	
}

var validateDate = function(date, formGroup, errorMessage){
	if(date != null && date.trim().length > 0){
		$(formGroup).addClass("has-success").removeClass("has-error");
		$(errorMessage).html("");
		return true;
	}else{
		$(formGroup).addClass("has-error").removeClass("has-success");
		$(errorMessage).html("A data deve ser preenchida!");
		return false;
	}
}

var membersOnTask = [];

var addMemberSelectEdit = function(member, task){
	var newMemberSelect = "<option value='"+member.email+"'>"+member.name+" ("+member.email+")"+"</option>";
	$("#selectMembers"+task.id).append(newMemberSelect);
}

var addMemberToTaskEdit = function(member, pos, task){
	var newMember = "<li id='member"+pos+"task"+task.id+"' class='member "+randomColor()+"'><i class='fa fa-ellipsis-v'></i><div class='task-title'><span class='task-title-sp'>"+member+"</span><div class='pull-right hidden-phone'><button type='button' id='removeEdit"+pos+"task"+task.id+"' class='btn btn-danger btn-xs fa fa-trash-o'></button></div></div></li>";
	$(".sortable"+task.id).append(newMember);
	$("#removeEdit"+pos+"task"+task.id).click(function(){
		$("#member"+pos+"task"+task.id).remove();
		task.membersOnTaskEdit[pos] = "";
	});
}



var loadTasks = function(idGroup){
	$("#addTaskBtn").fadeIn();
	membersOnTask = [];
	
	$("#taskGridster").html("");
	$("#editTaskModals").html("");
	$("#descriptionsZoom").html("");
	
	$.ajax({
		url:"../listTasks",
		data:{idGroup:idGroup},
		type:"get",
		dataType:"json",
		success:function(response){
			if(response == "error"){
				
			}else{
				for(var i=0; i<response.length; i++){
					addTask(response[i]);
				}
			}
		}
	});
}

var randomColor = function(){
	var color = "list-danger";
	return color;
}

var taskClass = function(task){
	if(!task.done){
		return 'task-por-fazer';
	}else{
		return 'task-feita';
	}
}

var addTask = function(task){
	
	isValidatedDescriptionEdit[task.id] = true;
	isValidatedTimeEdit[task.id] = true;
	isValidatedNameEdit[task.id] = true;
	isValidatedDateEdit[task.id] = true;
	
	var date = task.date.split(" ");
	task.date = date[0];
	task.time = date[1];
	
	date = task.date.split("-");
	
	task.dateToShow = [date[2],date[1],date[0]].join("-");
	
	console.log(task);
	if($("#task"+task.id).length > 0){
		console.log($("#task"+task.id).length);
		$("#task"+task.id).remove();
	}
	
	
	
	var newTask = '<li class="task-panel-gridster pn content-panel '+taskClass(task)+'" id="task'+task.id+'">'+
			'<div class="task">'+
				'<div class="white-header">'+
					'<h5>'+task.name+'</h5>'+
					'<a id="btn-marcar-feita'+task.id+'" class="btn btn-marcar-feita"><i class="fa fa-check"></i></a>'+
				'</div>'+
				'<div class="task-content">'+
					'<div class="zoom" id="zoom'+task.id+'" tilte="Ampliar a descrição">'+
						'<i class="fa fa-search lupe fa-5x"></i>'+
					'</div>'+
					'<h3 class="centered" id="description'+task.id+'">'+task.description+'</h3>'+
					'<div class="clearfix"></div>'+
					/*'<div class="responsavel" id="responsible'+task.id+'">'+
					'</div>'+*/
					'<div class="date">'+
                  		'<h5 class="centered">'+task.dateToShow+' <small>'+task.time+'hrs</small></h5>'+
                  	'</div>'+
					'<div class="options">'+
						'<a class="btn remove" title="Remover" id="remove'+task.id+'">'+
							'<i class="fa fa-trash-o"></i>'+
						'</a>'+ 
						'<a class="btn edit" title="Editar" data-toggle="modal" data-target="#editTaskModal'+task.id+'" id="edit'+task.id+'">'+
							'<i class="fa fa-edit"></i>'+
						'</a>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</li>';
	
	$("#taskGridster").append(newTask);
	
	$("#btn-marcar-feita"+task.id).click(function(){
		$.ajax({
			url:"../updateDoneTask",
			data:{taskId:task.id, done: !task.done},
			type:"post",
			dataType:"json",
			success:function(response){
				if(response == "error"){
					
				}else{
					task.done = response;
					if(response){
						$("#task"+task.id).removeClass("task-por-fazer").addClass("task-feita");
					}else{
						$("#task"+task.id).addClass("task-por-fazer").removeClass("task-feita");	
					}
				}
			}
		});
	});
	
	$("#task"+task.id).hover(
		function(){
			$('.options', this).stop().slideDown('fast');
			$('.date', this).stop().slideDown('fast');
	    	$('.zoom', this).stop().fadeIn('fast');
			$('.task .task-content h3', this).css({color:"#cdcdcd"});
	    },
	    function(){
	    	$('.options', this).stop().slideUp('fast');
	    	$('.date', this).stop().slideUp('fast');
	    	$('.zoom', this).stop().fadeOut('fast');
	    	$('.task .task-content h3', this).css({color:"#000"});
	    }
	 );  
	
	$('.date', "#task"+task.id).click(function(event){
		event.preventDefault();
	});
	
	var newModalDescription = '<div class="modal fade modalDescription" id="modalDescription'+task.id+'" tabindex="-1" role="dialog" aria-labelledby="editTaskModalLabel" aria-hidden="true">'+
									'<div class="modal-dialog">'+
										'<div class="modal-content">'+
											'<div class="modal-header">'+
												'<button type="button" class="close" data-dismiss="modal"'+
													'aria-hidden="true">&times;</button>'+
												'<h4 class="modal-title" id="editTaskMotalLabel">Descrição da tarefa '+task.name+'</h4>'+
											'</div>'+
											'<div class="modal-body">'+
												task.description+
											'</div>'+
											'<div class="modal-footer">'+
												'<button type="button" class="btn btn-default" id="close"'+
													'data-dismiss="modal">Fechar</button>'+
											'</div>'+
										'</div>'+
									'</div>'+
							  '</div>';
	
	
	
	$('#zoom'+task.id).click(function(){
		$("#modalDescription"+task.id).modal('show');
	});
	

	/*if(task.responsibles != undefined && task.responsibles.length > 0){
		var responsible = ['<img src="'+task.responsibles[0].profilePicturePATH+'" title="'+task.responsibles[0].name+'" alt="'+task.responsibles[0].name+'"',
		                   		'class="img-circle" width="50">',
		                   	'<div class="clearfix"></div>'].join('');
		$("#responsible"+task.id).html(responsible);
	}*/
	
	$("#remove"+task.id).click(function(){
		bootbox.confirm("Deseja realmente excluir essa tarefa?",function(result){
			if(result){
				$.ajax({
					url:"../removeTask",
					data:{idTask:task.id},
					type:"post",
					success:function(response){
						if(response == "success"){
							$("#task"+task.id).remove();
							$("#modalDescription"+task.id).remove();
							$("#editTaskModal"+task.id).remove();
						}else{
							
						}
					}
				});
			}
		});
	});
	
	task.membersOnTaskEdit = [];
	var newModalEditTask = ''+
	'<div class="modal fade" id="editTaskModal'+task.id+'" tabindex="-1"'+
		'role="dialog" aria-labelledby="editTaskModalLabel"'+
		'aria-hidden="true">'+
		'<div class="modal-dialog">'+
			'<div class="modal-content">'+
				'<div class="modal-header">'+
					'<button type="button" class="close" data-dismiss="modal"'+
						'aria-hidden="true">&times;</button>'+
					'<h4 class="modal-title" id="editTaskMotalLabel">Editar tarefa </h4>'+
				'</div>'+
				'<form id="editTaskForm'+task.id+'" class="form-horizontal" method="post" action="../editTask">'+
					'<div class="modal-body">'+
						'<div class="form-group" id="taskName-form-group'+task.id+'">'+
							'<label class="col-sm-3 col-sm-3 control-label">Título da tarefa:</label>'+
							'<div class="col-sm-9">'+
								'<input type="text" class="task-form-control form-control" value="'+task.name+'" id="taskName'+task.id+'" name="name">'+
								'<span class="help-block" id="taskName-error-message'+task.id+'"></span>'+
							'</div>'+
						'</div>'+
						'<div class="form-group" id="taskDescription-form-group'+task.id+'">'+
							'<label class="col-sm-3 col-sm-3 control-label">Descrição:</label>'+
							'<div class="col-sm-9">'+
								'<textarea class="task-form-control form-control resize"'+
									'name="taskDescription" id="taskDescription'+task.id+'">'+task.description+'</textarea>'+
									'<span class="help-block" id="taskDescription-error-message'+task.id+'"></span>'+
							'</div>'+
						'</div>'+
						'<div class="form-group" id="taskDate-form-group'+task.id+'">'+
							'<label class="col-sm-3 col-sm-3 control-label">Data:</label>'+
							'<div class="col-sm-9">'+
								'<input type="date" class="task-form-control form-control" id="taskDate'+task.id+'" value="'+task.date+'" name="date">'+
								'<span class="help-block" id="taskDate-error-message'+task.id+'"></span>'+
							'</div>'+
						'</div>'+
						'<div class="form-group id="taskTime-form-group'+task.id+'">'+
							'<label class="col-sm-3 col-sm-3 control-label">Horário:</label>'+
							'<div class="col-sm-9">'+
								'<input type="text" class="task-form-control form-control" id="taskTime'+task.id+'" value="'+task.time+'" name="time" placeholder="Ex.: 23:40">'+
								'<span class="help-block" id="taskTime-error-message'+task.id+'"></span>'+
							'</div>'+
						'</div>'+
						'<div class="form-group">'+
							'<section class="task-panel tasks-widget">'+
								'<div class="panel-heading">'+
									'<div class="pull-left">'+
										'<h5>'+
											'<i class="fa fa-user"></i> Responsáveis pela tarefa:'+
										'</h5>'+
									'</div>'+
									'<br>'+
								'</div>'+
								'<div class="panel-body">'+
									'<div class="task-content">'+
										'<ul id="sortable" class="task-list sortable'+task.id+'">'+
										'</ul>'+
											'<div class="col-sm-10">'+
												'<select class="task-form-control form-control" id="selectMembers'+task.id+'" name="addedToTask">'+
												'</select>'+
											'</div>'+
											'<div class="col-sm-2">'+
												'<input type="button" id="add'+task.id+'" value="add" class="btn-ht btn">'+
											'</div>'+
									'</div>'+
								'</div>'+
							'</section>'+
						'</div>'+
					'</div>'+
					'<div class="modal-footer">'+
						'<button type="button" class="btn btn-default" id="close"'+
							'data-dismiss="modal">Fechar</button>'+
						'<input type="submit" value="Salvar" class="btn btn-ht">'+
					'</div>'+
				'</form>'+
			'</div>'+
		'</div>'+
	'</div>';
	
	
	
	$("#close", "#editTaskModal"+task.id).click();
		
	$("#close", "#modalDescription"+task.id).click();
	
	
	
	setTimeout(function(){
		if($("#editTaskModal"+task.id).length > 0){
			$("#editTaskModal"+task.id).remove();
		}
		
		if($("#modalDescription"+task.id).length > 0){
			$("#modalDescription"+task.id).remove();
		}
		
		$("#descriptionsZoom").append(newModalDescription);
		$("#editTaskModals").append(newModalEditTask);			
		
		TaskList.initTaskWidget();
		$(".sortable"+task.id).sortable();
	    $(".sortable"+task.id).disableSelection();
	    autosize($('.resize', "#task"+task.id));
	    
		
		
		$.ajax({
			url:"../listMembers",
			data:{idGroup:idGroup},
			type:"post",
			dataType:"json",
			success:function(response){
				if(response == "error"){
					
				}else{
					$("#selectMembers"+task.id).html("<option></option>");
					for(var i = 0; i<response.length; i++){
						addMemberSelectEdit(response[i], task);
					}
				}
			}
		});
		
		$('#taskRedoCheckbox'+task.id).change(function(){
			if($(this).is(':checked')){
					$("#taskRedoTypeSelect"+task.id).prop( "disabled", false);
	        }else{
	            	$("#taskRedoTypeSelect"+task.id).prop( "disabled", true);
	        }
	    });
		
		
	    
		if(task.responsibles != undefined && task.responsibles.length > 0){
			for(var i=0; i<task.responsibles.length; i++){
				var pos = task.membersOnTaskEdit.length;
				task.membersOnTaskEdit[pos] = task.responsibles[i].email;
				addMemberToTaskEdit(task.responsibles[i].email, pos, task);
			}
		}
		
		$("#add"+task.id).click(function(){
			var val = $("#selectMembers"+task.id).val();
			if(val.trim().length == 0) return false;
			var isAdded = false;
			for(var i=0; i< task.membersOnTaskEdit.length; i++){
				if(task.membersOnTaskEdit[i] == val){
					isAdded = true;
					break;
				}
			}
			if(!isAdded){
				var pos = task.membersOnTaskEdit.length;
				task.membersOnTaskEdit[pos] = val;
				addMemberToTaskEdit(val, pos, task);
			}
		});
		
		$("#taskName"+task.id, "#editTaskForm"+task.id).keyup(function(){
			var name = $(this).val();
			isValidatedNameEdit[task.id] = validateName(name, "#taskName-form-group"+task.id, "#taskName-error-message"+task.id);
		}).blur(function(){
			var name = $(this).val();
			isValidatedNameEdit[task.id] = validateName(name, "#taskName-form-group"+task.id, "#taskName-error-message"+task.id);
		});
		
		$("#taskDescription"+task.id, "#editTaskForm"+task.id).keyup(function(){
			var description = $(this).val();
			isValidatedDescriptionEdit[task.id] = validateDescription(description, "#taskDescription-form-group"+task.id, "#taskDescription-error-message"+task.id);
		}).blur(function(){
			var description = $(this).val();
			isValidatedDescriptionEdit[task.id] = validateDescription(description, "#taskDescription-form-group"+task.id, "#taskDescription-error-message"+task.id);
		});
		
		$("#taskTime"+task.id, "#editTaskForm"+task.id).keyup(function(){
			var time = $(this).val();
			isValidatedTimeEdit[task.id] = validateTime(time, "#taskTime-form-group"+task.id, "#taskTime-error-message"+task.id);
		}).blur(function(){
			var time = $(this).val();
			isValidatedTimeEdit[task.id] = validateTime(time, "#taskTime-form-group"+task.id, "#taskTime-error-message"+task.id);
		});
		
		$("#taskDate"+task.id, "#editTaskForm"+task.id).keyup(function(){
			var date = $(this).val();
			isValidatedDateEdit[task.id] = validateDate(date, "#taskDate-form-group"+task.id, "#taskDate-error-message"+task.id);
		}).blur(function(){
			var date = $(this).val();
			isValidatedDateEdit[task.id] = validateDate(date, "#taskDate-form-group"+task.id, "#taskDate-error-message"+task.id);
		});
		
		$("#editTaskForm"+task.id).submit(function(event){
			event.preventDefault();
			var responsibles = task.membersOnTaskEdit.join(",");
			var description = $("#taskDescription"+task.id).val();
			var name = $("#taskName"+task.id).val();
			var date = $("#taskDate"+task.id).val();
			var time = $("#taskTime"+task.id).val();
			
			if(isValidatedNameEdit[task.id] && isValidatedDescriptionEdit[task.id] && isValidatedTimeEdit[task.id] && isValidatedDateEdit[task.id]){
	
				$.ajax({
					url:"../editTask",
					type:"post",
					dataType:"json",
					data:{idTask:task.id, name:name, description:description, responsibles:responsibles, date:date, time:time, idGroup:idGroup, done:task.done},
					success:function(response){
						console.log(response);
						if(response == "error"){
							
						}else{
							addTask(response);
						}
					}
				});
			}
			return false;
		});
	}, 400);
}

var addMemberToTask = function(member, pos){
	pos--;
	var newMember = "<li id='member"+pos+"' class='"+randomColor()+"'><i class='fa fa-ellipsis-v'></i><div class='task-title'><span class='task-title-sp'>"+member+"</span><div class='pull-right hidden-phone'><button id='remove"+pos+"' class='btn btn-danger btn-xs fa fa-trash-o'></button></div></div></li>";
	$("#sortable").append(newMember);
	$("#remove"+pos).click(function(){
		$("#member"+pos).remove();
		membersOnTask[pos] = "";
	});

}

var addMemberSelect = function(member){
	var newMemberSelect = "<option value='"+member.email+"'>"+member.name+" ("+member.email+")"+"</option>";
	$("#selectMembers").append(newMemberSelect);
}



$(document).ready(function(){
	$("#taskName", "#addTaskForm").keyup(function(){
		var name = $(this).val();
		isValidatedName = validateName(name, "#taskName-form-group", "#taskName-error-message");
	}).blur(function(){
		var name = $(this).val();
		isValidatedName = validateName(name, "#taskName-form-group", "#taskName-error-message");
	});
	
	$("#taskDescription", "#addTaskForm").keyup(function(){
		var description = $(this).val();
		isValidatedDescription = validateDescription(description, "#taskDescription-form-group", "#taskDescription-error-message");
	}).blur(function(){
		var description = $(this).val();
		isValidatedDescription = validateDescription(description, "#taskDescription-form-group", "#taskDescription-error-message");
	});
	
	$("#taskTime", "#addTaskForm").keyup(function(){
		var time = $(this).val();
		isValidatedTime = validateTime(time, "#taskTime-form-group", "#taskTime-error-message");
	}).blur(function(){
		var time = $(this).val();
		isValidatedTime = validateTime(time, "#taskTime-form-group", "#taskTime-error-message");
	});
	
	$("#taskDate", "#addTaskForm").keyup(function(){
		var date = $(this).val();
		isValidatedDate = validateDate(date, "#taskDate-form-group", "#taskDate-error-message");
	}).blur(function(){
		var date = $(this).val();
		isValidatedDate = validateDate(date, "#taskDate-form-group", "#taskDate-error-message");
	});
	
	$("#addTaskForm").submit(function(event){
		event.preventDefault();
		var responsibles = membersOnTask.join(",");
		var description = $("#taskDescription").val();
		var name = $("#taskName").val();
		var date = $("#taskDate").val();
		var time = $("#taskTime").val();
		
		if(isValidatedName && isValidatedDescription && isValidatedTime && isValidatedDate){

			$.ajax({
				url:"../addTask",
				type:"post",
				dataType:"json",
				data:{name:name, description:description, responsibles:responsibles, date:date, time:time, idGroup:idGroup},
				success:function(response){
					console.log(response);
					if(response == "error"){
						
					}else{
						addTask(response);
						$("#close", "#addTaskModal").click();
					}
				}
			});
		}
		return false;
	});
	
	$("#add").click(function(){
		var val = $("#selectMembers").val();
		if(val.trim().length == 0) return false;
		var isAdded = false;
		for(var i=0; i<membersOnTask.length; i++){
			if(membersOnTask[i] == val){
				isAdded = true;
				break;
			}
		}
		if(!isAdded){
			membersOnTask[membersOnTask.length] = val;
			addMemberToTask(val, membersOnTask.length);
		}
	});
	
	
	$("#addTaskBtn").click(function(){
		$.ajax({
			url:"../listMembers",
			data:{idGroup:idGroup},
			type:"post",
			dataType:"json",
			success:function(response){
				if(response == "error"){
					
				}else{
					$("#selectMembers").html("<option></option>");
					for(var i = 0; i<response.length; i++){
						addMemberSelect(response[i]);
					}
				}
			}
		});
	});
});