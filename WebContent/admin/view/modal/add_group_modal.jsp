<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="modal fade" id="addGroup" tabindex="-1" role="dialog"
	aria-labelledby="addGroupModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="addTaskMotalLabel">Adicionar Grupo:</h4>
			</div>
			<form id="addGroupForm" class="form-horizontal" action="addGroup" method="post">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label">Nome do
							Grupo:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="name" name="name"
								value="">
							<div class='display-none has-error' id="error-message">Nome inválido!</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="submit" id="addGroupBtn" class="btn btn-ht">Adicionar</button>
				</div>
			</form>
		</div>
	</div>
</div>



