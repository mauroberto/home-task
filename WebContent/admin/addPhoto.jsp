<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Adicionar Foto</title>

<!-- Bootstrap core CSS -->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="../assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="../assets/css/zabuto_calendar.css">
<link rel="stylesheet" type="text/css"
	href="../assets/js/gritter/css/jquery.gritter.css" />
<link rel="stylesheet" type="text/css"
	href="../assets/lineicons/style.css">
<link href="../assets/css/hometask.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link rel="stylesheet" type="text/css"
	href="../assets/gridster/dist/jquery.gridster.css">
<link href="../assets/css/style.css" rel="stylesheet">
<link href="../assets/css/style-responsive.css" rel="stylesheet">
</head>
<body>
	<!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
		<!--header start-->
		
		<!--header start-->
		<%@include file="view/header.jsp"%>
		<!--header end-->
		<!--main content start-->
		
		<div class="col-lg-12">
			<section class="wrapper">
				<h3>
					<i class="fa fa-angle-right"></i> Configurações
				</h3>
				<div class="row mt">
					<div class="col-lg-12">
						<div class="form-panel">
							<form action="addPhoto" method="post" enctype="multipart/form-data">
								<h3>ADICIONAR FOTO</h3>
								<input class="form-group" type="file" name="file">
								<input type="submit" value="Enviar" class="btn btn-ht">
								<a class="btn btn-default" href="index.jsp"><i class="fa fa-angle-left"></i> Voltar</a>
							</form>
						</div>
					</div>
					<!-- col-lg-12-->
				</div>
				<div class="clearfix"></div>
			</section>
		</div>
			<!-- /MAIN CONTENT -->

			<!--main content end-->
			<!--footer start-->
			<%@include file="view/footer.jsp"%>
			<!--footer end-->
</body>
<!-- js placed at the end of the document so the pages load faster -->
	<script src="../assets/js/jquery.js"></script>
	<script src="../assets/js/jquery-1.8.3.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript"
		src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="../assets/js/jquery.scrollTo.min.js"></script>
	<script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="../assets/js/jquery.sparkline.js"></script>
	<script type="text/javascript" src="view/js/uploadPhoto.js"></script>
</html>