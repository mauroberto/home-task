<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword"
	content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

<title>HomeTask - Editar Perfil</title>

<!-- Bootstrap core CSS -->
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="../assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="../assets/css/zabuto_calendar.css">
<link rel="stylesheet" type="text/css"
	href="../assets/js/gritter/css/jquery.gritter.css" />
<link rel="stylesheet" type="text/css"
	href="../assets/lineicons/style.css">
<link href="../assets/css/hometask.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link rel="stylesheet" type="text/css"
	href="../assets/gridster/dist/jquery.gridster.css">
<link href="../assets/css/style.css" rel="stylesheet">
<link href="../assets/css/style-responsive.css" rel="stylesheet">



<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

		<!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
		<!--header start-->
		
		<!--header start-->
		<%@include file="view/header.jsp"%>
		<!--header end-->
		<!--main content start-->
		
		<div class="col-lg-12">
			<section class="wrapper">
				<h3>
					<i class="fa fa-angle-right"></i> Configurações
				</h3>
				<div class="row mt">
					<div class="col-lg-12">
						<div class="form-panel">
							<h4 class="mb">
								<i class="fa fa-angle-right"></i> Atualizar Informações Pessoais
							</h4>
							<form id="update_perfil" class="form-horizontal style-form" method="post"
								action="../UpdatePersonalInformation">
								<input type="hidden" value="${person.id}" name="id">
								<c:if test="${modified == 1}">
									<div class="alert alert-success">
										<b>Pronto!</b> Suas informações foram atualizadas.
									</div>
								</c:if>
								<c:if test="${modified == 0 and not empty errors}">
									<c:forEach var="error" items="${errors}">
										<div class="alert alert-danger">
											<b>Ops!</b> ${error}
										</div>
									</c:forEach>
								</c:if>
								<div class="form-group">
									<label class="col-sm-2 col-sm-2 control-label">Nome</label>
									<div class="col-sm-10">
										<input id="name-input" name="name" type="text" class="form-control"
											value="${person.name}">
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
										<input id="email-input" name="email" type="text" class="form-control"
											value="${person.email}">
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 col-sm-2 control-label">Senha</label>
									<div class="col-sm-10">
										<input id="password-input" name="password" type="password" class="form-control">
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 col-sm-2 control-label">Confirmação
										de Senha</label>
									<div class="col-sm-10">
										<input id="password-conf-input" type="password" name="passwordConfirm" class="form-control">
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-10">
										<input id="signup-button" type="submit" value="Atualizar" class="btn btn-ht">
										<a class="btn btn-default" href="index.jsp"><i class="fa fa-angle-left"></i> Voltar</a>
									</div>
								</div>

							</form>
						</div>
					</div>
					<!-- col-lg-12-->
				</div>
				
				<c:set var="modified" scope="session" value="${0}"></c:set>
				</section>
			</div>
			<!-- /MAIN CONTENT -->

			<!--main content end-->
			<!--footer start-->
			<%@include file="view/footer.jsp"%>
			<!--footer end-->

	<!-- js placed at the end of the document so the pages load faster -->
	<script src="../assets/js/jquery.js"></script>
	<script src="../assets/js/jquery-1.8.3.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript"
		src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="../assets/js/jquery.scrollTo.min.js"></script>
	<script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="../assets/js/jquery.sparkline.js"></script>


	<!--common script for all pages-->
	<script src="../assets/js/common-scripts.js"></script>

	<script type="text/javascript"
		src="../assets/js/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="../assets/js/gritter-conf.js"></script>

	<!--script for this page-->
	<script src="../assets/js/sparkline-chart.js"></script>
	<script src="../assets/js/zabuto_calendar.js"></script>

	<script type="text/javascript"
		src="../assets/gridster/dist/jquery.gridster.js"></script>
	<script src="view/modal/js/deactivate_account_modal.js"></script>
	<script src="view/modal/js/add_member_modal.js"></script>
	<script src="view/modal/js/add_group_modal.js"></script>
	<script src="../assets/js/chart-master/Chart.js"></script>
	<script src="../js/sha512.js"></script>
	<script src="view/js/updatePerfil.js"></script>
</body>
</html>