<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="HomeTask">
<meta name="keyword" content="HomeTask">

<title>HomeTask - Login</title>

<!-- Bootstrap core CSS -->
<link href="assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	<div id="login-page">
		<div class="container">

			<form class="form-login" id="login-form" method="POST" action="">
				<h2 class="form-login-heading">Realizar Login</h2>
				<div class="login-wrap">
					<input type="text" class="form-control" id="email" name="email"
						placeholder="exemplo@email.com" autofocus> <br> <input
						type="password" class="form-control" name="password" id="password"
						placeholder="Senha"> <label class="checkbox"> <span
						class="pull-right"> 

					</span>
					</label>
					<button class="btn btn-theme btn-block" type="submit"
						id="login-button">
						<i class="fa fa-lock"></i>Entrar
					</button>
					<hr>
					<div id="error-message"></div>
					<div class="registration">
						N�o possui uma conta ainda?<br /> <a class="" href="signup.jsp">
							Criar Conta </a>
					</div>
					<div class="registration">
						<a class="" href="recover_password.jsp">
							Recuperar Senha </a>
					</div>

				</div>

			</form>

		</div>
	</div>



	<!-- js placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<script src="js/sha512.js"></script>
	<script src="js/login.js"></script>



</body>
</html>
