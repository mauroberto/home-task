var beforeSubmit = function(){
	
	
	var emailIsValid = false;
	var nameIsValid = false;
	var passwordIsValid = false;
	var passwordConfirmationIsValid = false;
	
	if(validateEmail($("#email-input").val())){
		$("#email-form").removeClass("has-error");
		$("#email-error-message").html("");
		emailIsValid = true;
	}else if($("#email-input").val().trim().length == 0){
		$("#email-form").addClass("has-error").removeClass("has-success");
		$("#email-error-message").html("O email &eacute; obrigat&oacute;rio!");
	}else{
		$("#email-form").addClass("has-error").removeClass("has-success");
		$("#email-error-message").html("Insira um email v&aacute;lido!");
	}
	
	
	if(validateName($("#name-input").val())){
		$("#name-form").removeClass("has-error");
		$("#name-error-message").html("");
		nameIsValid = true;
	}else{
		$("#name-form").addClass("has-error").removeClass("has-success");
		$("#name-error-message").html("O nome &eacute; obrigat&oacute;rio!");
	}
	
	if(validatePassword($("#password-input").val())){
		$("#password-form").removeClass("has-error");
		$("#pass-error-message").html("");
		passwordIsValid = true;
	}else{
		$("#password-form").addClass("has-error").removeClass("has-success");
		$("#pass-error-message").html("A senha deve ter no m&iacute;nimo 6 caracteres!");
	}
	
	if(confirmPasswordEquals($("#password-input").val(),$("#password-conf-input").val())){
		$("#password-conf-form").removeClass("has-error");
		$("#pass-conf-error-message").html("");
		passwordConfirmationIsValid = true;
	}else{
		$("#password-conf-form").addClass("has-error").removeClass("has-success");
		$("#pass-conf-error-message").html("A confirma&ccedil;&atilde;o de senha &eacute; obrigat&oacute;ria e deve ser igual a senha digitada!");
	}

	if(emailIsValid && nameIsValid && passwordIsValid && passwordConfirmationIsValid){
		
		var password = $("#password-input").val();
		
		$("#password-input").val(password);
		
		return true;
	} else{
		return false;
	} 
	
}



$(document).ready(function(){
	
	$("#form-login").submit(function(){
		if(!beforeSubmit()) return false;
	});
	
	$("#email-input").keyup(function(e){
		if(e.which > 90 || e.which < 48){
			return;
		}
		var email = $(this).val().trim();
		ajaxEmail(email);
	}).blur(function(){
		var email = $(this).val().trim();
		ajaxEmail(email);
	});
	
	var ajaxEmail = function(email){
		
		if(validateEmail(email)){
			
			
				
				$.ajax({
					url:"emailVerifyer",
					type:"post",
					data:{email:email},
					beforeSend:function(){
						$("#email-form").addClass("has-warning").removeClass("has-success").removeClass("has-error");
						$("#email-error-message").html("Verificando email na base de dados...  <i class='fa fa-refresh fa-spin'></i>");
					},
					success:function(retorno){
						
						if(retorno == "true"){
							$("#email-form").addClass("has-success").removeClass("has-warning");
							$("#email-error-message").html("Email dispon&iacute;vel para cadastro!");
						}else{
							$("#email-form").addClass("has-error").removeClass("has-warning");
							$("#email-error-message").html("Esse email j&aacute; est&aacute; cadastrado!");
							
						}
					}
				});
			
			
		}else{
			$("#email-form").addClass("has-error").removeClass("has-success");
			$("#email-error-message").html("Insira um email v&aacute;lido!");
		}
	}
	
	$("#password-input").keyup(function(){
		passwordValidate($(this).val());
	}).blur(function(){
		passwordValidate($(this).val());
	});
	
	var passwordValidate = function(password){
		if(validatePassword(password)){
			$("#password-form").addClass("has-success").removeClass("has-error");
			$("#pass-error-message").html("");
		}else{
			$("#password-form").addClass("has-error").removeClass("has-success");
			$("#pass-error-message").html("A senha deve ter no m&iacute;nimo 6 caracteres!");
		}
	}
	
	$("#password-conf-input").keyup(function(){
		passwordConfValidate($(this).val());
	}).blur(function(){
		passwordConfValidate($(this).val());
	});
	
	var passwordConfValidate = function(passwordConf){
		if(confirmPasswordEquals($("#password-input").val(),passwordConf)){
			$("#password-conf-form").addClass("has-success").removeClass("has-error");
			$("#pass-conf-error-message").html("");
		}else{
			$("#password-conf-form").addClass("has-error").removeClass("has-success");
			$("#pass-conf-error-message").html("A confirma&ccedil;&atilde;o de senha deve ser igual a senha digitada e n&atilde;o pode ser vazia!");
		}
	}
	
	$("#name-input").keyup(function(){
		nameValidate($(this).val());
	}).blur(function(){
		nameValidate($(this).val());
	});
	
	var nameValidate = function(name){
		if(validateName(name)){
			$("#name-form").addClass("has-success").removeClass("has-error");
			$("#name-error-message").html("");
		}else{
			$("#name-form").addClass("has-error").removeClass("has-success");
			$("#name-error-message").html("O nome &eacute; obrigat&oacute;rio!");
		}
	}
});

var validateName = function(name){
	return name.trim().length > 0;
}

var validateEmail = function(email){
	var regexp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(email.trim().lenght == 0 || !regexp.test(email)){
		return false;
	}
	return true;
}

var validatePassword = function(password){
	if(password.trim().length < 6){
		return false;
	}
	return true;
}

var confirmPasswordEquals = function(password,confirmation){
	if(password == confirmation && confirmation.trim().length >= 6){
		return true;
	}
	return false
}