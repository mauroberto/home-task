$(document).ready(function(){
	
	if($.urlParam("error") == "true"){
		var erroComponent = "<div class='alert alert-danger'><b>Email</b> ou <b>senha</b> inválidos.</div>";
		$("#error-message").html(erroComponent);
	}
	
	$("#login-form").submit(function(){
		
		var email = $("#email").val();
		
		$.ajax({
			url:"recover",
			type:"post",
			data:{email:email},
			beforeSend:function(){
				$("#login-button").html("<i class='fa fa-lock'></i>Entrar <i class='fa fa-refresh fa-spin'></i>");
			},
			success:function(retorno){
				$("#login-button").html("<i class='fa fa-lock'></i>Entrar");
				console.log(retorno);
				if(retorno == "true"){
					window.location = "admin";
				}else{
					var erroComponent = "<div class='alert alert-danger'><b>Email</b> inv&aacute;lido.</div>";
					$("#error-message").html(erroComponent);
				}
			}
		});
		
		return false;
	
	});
});


$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}



