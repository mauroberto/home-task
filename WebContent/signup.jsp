<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="HomeTask">
    <meta name="keyword" content="hometask">

    <title>Cadastre-se - HomeTask</title>

    <!-- Bootstrap core CSS -  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/hometask.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
      
      
     
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
    	<div id="login-page">
	  	<div class="container">
	  	
		      <form id="form-login" class="form-login" action="addPerson" method="post">
		        <h2 class="form-login-heading">Cadastre-se agora</h2>
		        <div class="login-wrap">
		           	<div class="form-group" id="name-form">
                    	<input type="text" placeholder="Nome" name="name" id="name-input" class="form-control">
                    	<span class="help-block" id="name-error-message"></span>
					</div>
					<div class="form-group" id="email-form">
						<input type="text" placeholder="Email" name="email" id="email-input" class="form-control">
						<span class="fa fa-spinner form-control-feedback hidden" aria-hidden="true"></span>
                        <span class="help-block" id="email-error-message"></span>
					</div>
					<div class="form-group" id="password-form">
						<input type="password" placeholder="Senha" name="password" id="password-input" class="form-control">
						<span class="help-block" id="pass-error-message"></span>
					</div>
					<div class="form-group" id="password-conf-form">
						<input type="password" placeholder="Confirma��o de Senha" id="password-conf-input" class="form-control">
                        <span class="help-block" id="pass-conf-error-message"></span>
					</div>
                    <input type="submit" class="centered btn btn-theme btn-block" id="signup-button" value="Cadastrar-se"/>
		           	</hr>
		           	
		            <div class="registration">
		                J� possui cadastro?<br/>
		                <a href="login.jsp">
		                    Fa�a o login
		                </a>
		            </div>
		        </div>
		
		      </form>	  	
	  	
	  	</div>
	  </div>
    	

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 - HomeTask
          </div>
      </footer>
      <!--footer end-->

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom switch-->
	<script src="assets/js/bootstrap-switch.js"></script>
	
	<!--custom tagsinput-->
	<script src="assets/js/jquery.tagsinput.js"></script>
	
	<!--custom checkbox & radio-->
	
	<script type="text/javascript" src="js/sha512.js"></script>
	<script type="text/javascript" src="js/signup.js"></script> 
    

  </body>
</html>
