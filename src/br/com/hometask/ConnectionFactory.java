package br.com.hometask;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionFactory implements IConnectionFactory{
	
	String driver = "com.mysql.jdbc.Driver";
	String url = "jdbc:mysql://localhost:3306/hometask";
	String user = "root";
	String password = "root";
	

	public Connection getConnection(){
		
		Connection connection = null;
		
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url,user,password);
		} catch (ClassNotFoundException e) {
			System.out.println("Classe não encontrada!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
}
