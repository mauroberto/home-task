package br.com.hometask.task;

import java.util.List;

import br.com.hometask.group.Group;
import br.com.hometask.person.Person;

public interface ITaskController {
	
	public Task addTask(Task task);
	
	public void assingTaskToAPerson(Task task, Person person);
	
	public List<Task> getAllTaskOfAPerson(Person person);
	
	public void update(Task task);
	
	public void removeTask(Task task);
	
	public Task getTaskById(long id);

	List<Task> getAllTaskOfAGroup(Group group);


}
