package br.com.hometask.task;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Properties;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

public class ReminderController implements IReminderController{

	private Scheduler scheduler;
	
	public ReminderController() {
		
		Properties props = new Properties();
		
		props.put("org.quartz.scheduler.instanceName", "DefaultQuartzScheduler");
		props.put("org.quartz.scheduler.rmi.export", "false");
		props.put("org.quartz.scheduler.rmi.proxy", "false");
		props.put("org.quartz.scheduler.wrapJobExecutionInUserTransaction", "false");
		
		props.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
		props.put("org.quartz.threadPool.threadCount", "10");
		props.put("org.quartz.threadPool.threadPriority", "5");
		props.put("org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread", "true");
		
		
		props.put("org.quartz.jobStore.misfireThreshold", "60000");
		props.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
		props.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
		props.put("org.quartz.jobStore.tablePrefix", "QRTZ_");
		props.put("org.quartz.jobStore.dataSource", "quartzDataSource");
		
		
		props.put("org.quartz.dataSource.quartzDataSource.driver", "com.mysql.jdbc.Driver");
		props.put("org.quartz.dataSource.quartzDataSource.URL", "jdbc:mysql://localhost:3306/hometask");
		props.put("org.quartz.dataSource.quartzDataSource.user", "root");
		props.put("org.quartz.dataSource.quartzDataSource.password", "123");
		props.put("org.quartz.dataSource.quartzDataSource.maxConnections", "20");

			
		try {
			
			scheduler = new StdSchedulerFactory(props).getScheduler();
			
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void createReminder(Task task) {
		
		JobDetail jobDetail = newJob(ReminderJob.class)
				.withIdentity(String.valueOf(task.getId()), "tasks")
				.usingJobData("task_id", task.getId())
				.build();
		
		SimpleTrigger trigger = (SimpleTrigger) newTrigger().withIdentity(String.valueOf(task.getId()))
				.startAt(task.getDate()).forJob(jobDetail).build();
		
		
		try {
			
			scheduler.start();
			
			scheduler.scheduleJob(jobDetail, trigger);
			
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void removeReminder(Task task) {
		
		try {
			scheduler.unscheduleJob(new TriggerKey(String.valueOf(task.getId()), "tasks"));
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
