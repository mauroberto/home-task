package br.com.hometask.task;

import java.util.List;

import br.com.hometask.group.Group;
import br.com.hometask.person.Person;

public class TaskController implements ITaskController{
	
	ITaskDAO taskDAO;
	IReminderController reminderController;
	
	public TaskController() {
		this.taskDAO = new TaskDAO();
		reminderController = new ReminderController();
	}

	@Override
	public Task addTask(Task task) {
		
		Task taskWhitId = taskDAO.insert(task);
		
		reminderController.createReminder(taskWhitId);
		
		return taskWhitId;
	}

	@Override
	public void assingTaskToAPerson(Task task, Person person) {
		this.taskDAO.insertResponsible(person, task);
		
	}

	@Override
	public List<Task> getAllTaskOfAPerson(Person person) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Task> getAllTaskOfAGroup(Group group){
		
		List<Task> tasks = taskDAO.getByGroup(group);
		
		for(Task task : tasks){
			
			for(Person person : task.getResponsibles()){
				
				person.setPassword(null);
			}
		}
		
		return tasks;
	}
	

	@Override
	public void update(Task task) {
		
		Task old = this.taskDAO.getOne(task.getId());
		this.taskDAO.update(task);
		
		for(Person person : task.getResponsibles()){
			if(!old.getResponsibles().contains(person)){
				this.taskDAO.insertResponsible(person, task);
			}
		}
		
		for(Person person : old.getResponsibles()){
			if(!task.getResponsibles().contains(person)){
				this.taskDAO.deleteResponsabilities(task, person);
			}
		}
	}

	@Override
	public void removeTask(Task task) {
		taskDAO.delete(task);
		reminderController.removeReminder(task);
		
	}

	@Override
	public Task getTaskById(long id) {
		return taskDAO.getOne(id);
	}

}
