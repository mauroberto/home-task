package br.com.hometask.task;

import java.util.List;

import br.com.hometask.group.Group;
import br.com.hometask.person.Person;

public interface ITaskDAO {

	public Task insert(Task task);
	public Task getOne(Long taskId);
	public void update(Task task);
	public void insertResponsible(Person person, Task task);
	public void delete(Task task);
	public void deleteResponsabilities(Task task, Person person);
	public List<Task> getByGroup(Group group);
	

}
