package br.com.hometask.task;

public interface IReminderController {
	
	public void createReminder(Task task);
	public void removeReminder(Task task);
}
