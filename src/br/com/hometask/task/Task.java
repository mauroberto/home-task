package br.com.hometask.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.hometask.group.Group;
import br.com.hometask.person.Person;

public class Task{

	private long id;
	private String name;
	private String description;
	private Group group;
	private Date date;
	private List<Person> responsibles;
	private boolean done;
	
	
	public Task() {
		responsibles = new ArrayList<Person>();
	}
	
	
	public Group getGroup() {
		return group;
	}


	public void setGroup(Group group) {
		this.group = group;
	}


	public boolean isDone(){
		return this.done;
	}
	
	public void setDone(boolean done){
		this.done = done;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public void addResponsible(Person person) {
		
		responsibles.add(person);
		
	}
	
	public List<Person> getResponsibles(){
		return responsibles;
	}

	
	
}
