package br.com.hometask.task.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.hometask.task.ITaskController;
import br.com.hometask.task.Task;
import br.com.hometask.task.TaskController;

/**
 * Servlet implementation class RemoveTask
 */
@WebServlet("/removeTask")
public class RemoveTask extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ITaskController taskController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveTask() {
        super();
        this.taskController = new TaskController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String json = "error";
		
		try{
			long idTask = Long.parseUnsignedLong(request.getParameter("idTask"));
			Task task = taskController.getTaskById(idTask);
			
			if(task != null){
				taskController.removeTask(task);
				json= "success";
			}
		}catch(RuntimeException e){
			
		}
		response.getWriter().write(json);
	}

}
