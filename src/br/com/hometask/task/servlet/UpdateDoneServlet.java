package br.com.hometask.task.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.task.ITaskController;
import br.com.hometask.task.Task;
import br.com.hometask.task.TaskController;

/**
 * Servlet implementation class UpdateDoneServlet
 */
@WebServlet("/updateDoneTask")
public class UpdateDoneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ITaskController taskController;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateDoneServlet() {
        super();
       this.taskController = new TaskController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String responseMessage = "error";

		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String taskId = request.getParameter("taskId");
		String done = request.getParameter("done");
		
		if(done != null && taskId != null){
			
			try{
				Task task = this.taskController.getTaskById(Long.parseLong(taskId));
				
				if(task != null){
					 if (done.equalsIgnoreCase("true")) {
						task.setDone(true);
					} else {
						task.setDone(false);
					}
					 
					 taskController.update(task);
					 
					 responseMessage = objectWriter.writeValueAsString(task.isDone());
					 
				}
			}catch(RuntimeException e){
				
			}
		}
		response.getWriter().write(responseMessage);
	}

}
