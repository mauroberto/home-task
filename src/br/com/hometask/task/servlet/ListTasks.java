package br.com.hometask.task.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.GroupDAO;
import br.com.hometask.group.IGroupController;
import br.com.hometask.group.IGroupDAO;
import br.com.hometask.task.ITaskController;
import br.com.hometask.task.Task;
import br.com.hometask.task.TaskController;

/**
 * Servlet implementation class ListTasks
 */
@WebServlet("/listTasks")
public class ListTasks extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ITaskController taskController;
	
	private IGroupController groupController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListTasks() {
        super();
        taskController = new TaskController();
        groupController = new GroupController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		

		ObjectWriter objectWriter = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();

		String json = "error";

		String idGroupParam = request.getParameter("idGroup");
		
		long idGroup = 0;
		
		try{
			
			idGroup = Long.parseLong(idGroupParam);
			
		}catch(NumberFormatException e){
			
			e.printStackTrace();
		}
		
		Group group = groupController.getGroupById(idGroup);
		
		List<Task> tasks = null;
		
		if(group != null){
		
			tasks = taskController.getAllTaskOfAGroup(group);

		}

		if (tasks != null) {
			json = objectWriter.withDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm")).writeValueAsString(tasks);
		}
		response.getWriter().write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

}
