package br.com.hometask.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.hometask.person.Person;

/**
 * Servlet Filter implementation class LogedFilter
 */
@WebFilter(filterName = "LoggedFilter", urlPatterns = {"/login.html","/index.html","/detalhes.jsp"}) 
public class LoggedFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoggedFilter() {
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpSession session = ((HttpServletRequest)request).getSession(false);
		
		if(session != null){
			Person person = (Person) session.getAttribute("person");
			
			if(person != null){
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect("admin/");
				return;
			}
			chain.doFilter(request, response);
			return;
		}else{
			
			chain.doFilter(request, response);
			return;
		}

		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
