package br.com.hometask;

import br.com.hometask.group.Group;
import br.com.hometask.person.Person;
import br.com.hometask.task.Task;

public interface IDataValidator {

	public boolean isEmailValid(String email);
	public boolean isPersonNameValid(String name);
	
	/**
	 * 
	 * Validates the Persons email, password, name in accordance with
	 * the buisness rules. See buisness rules documentation for more information.
	 * 
	 * @param person
	 * @return true if all the information is valid, false otherwise
	 */
	public boolean isPersonInformationValid(Person person);
	
	//I think is not efficient check if all the informations are valid all time. 
	//Maybe a method for each field could be better.
	//For example, to create a group it is necessary validate only if the name has a valid value.
	public boolean isGroupInformationValid(Group group);
	public boolean isTaskInformationValid(Task task);
}
