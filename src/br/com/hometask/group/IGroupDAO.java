package br.com.hometask.group;

import java.util.List;

import br.com.hometask.person.Person;

public interface IGroupDAO {

	public Group insert(Group group);
	public Group getBy(long id);
	public List<Group> getGroupsByOwner(Person owner);
	public List<Group> getAllGroupsOfAMember(Person member);
	public void update(Group group);
	public void delete(Group group);
	
}
