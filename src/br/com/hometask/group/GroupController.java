package br.com.hometask.group;

import java.util.List;

import br.com.hometask.person.IPersonDAO;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonDAO;

public class GroupController implements IGroupController {

	IGroupDAO groupDAO;
	IGroupMemberDAO groupMemberDAO;
	IPersonDAO personDAO;
	
	public GroupController() {
		this.groupDAO = new GroupDAO();
		this.groupMemberDAO = new GroupMemberDAO();
		this.personDAO = new PersonDAO();
	}
	
	@Override
	public Group addGroup(Group group, Person owner) {
		group.setCreator_id(owner.getId());
		group = this.groupDAO.insert(group);
		this.addMemberToGroup(group, owner);
		return group;
	}

	@Override
	public void addMemberToGroup(Group group, Person person) {
		this.groupMemberDAO.insert(group, person);
	}

	@Override
	public boolean removeMember(Group group, Person remover, Person removed) throws GroupPermissionException {
		
		if(this.hasPermission(group, remover, PermissionEnum.REMOVE_MEMBER)){
			return this.groupMemberDAO.remove(group, removed);
		} else {
			throw new GroupPermissionException("Member does not have permition to remover other member.");
		}
	}
	
	private boolean hasPermission(Group group, Person person, PermissionEnum operation){
		return true;
		//:TODO
	}

	@Override
	public Group getGroupById(Long id) {
		Group group =  this.groupDAO.getBy(id);
		
		if(group == null)
			return null;
		
		for(long memberID : groupMemberDAO.getAllMembers(group)){
			Person member = this.personDAO.getByID(memberID);
			member.setPassword("");
			group.addMember(member);
		}
		return group;
	}

	@Override
	public List<Group> getGroupsByOwner(Person owner) {
		return this.groupDAO.getGroupsByOwner(owner);
	}

	@Override
	public List<Group> getAllGroupsOfAMember(Person member) {
		return this.groupDAO.getAllGroupsOfAMember(member);
	}


	@Override
	public void updateGroup(Group group) {
		
		Group old = this.groupDAO.getBy(group.getId());

		groupDAO.update(group);
		
		for(Person person: group.getMembers()){
			if(!old.getMembers().contains(person)){
				this.groupMemberDAO.insert(group, person);
			}
		}
		
		for(Person person: old.getMembers()){
			if(!group.getMembers().contains(person)){
				this.groupMemberDAO.remove(group, person);
			}
		}
	}
	public List<Group> getAllPersonsGroups(Person person) {
		List<Group> groups = this.groupDAO.getGroupsByOwner(person);
		for(Long groupID : this.groupMemberDAO.getAllByMember(person)){
			Group group = this.getGroupById(groupID);
			groups.add(group);
		}
		return groups;
	}

	@Override
	public void removeGroup(Group group) {
		this.groupDAO.delete(group);
	}

}
