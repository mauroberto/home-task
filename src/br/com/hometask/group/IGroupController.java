package br.com.hometask.group;

import java.util.List;

import br.com.hometask.person.Person;

public interface IGroupController {

	public Group addGroup(Group group, Person owner);
	public void addMemberToGroup(Group group, Person person);
	public boolean removeMember(Group group, Person remover, Person removed) throws GroupPermissionException;
	public Group getGroupById(Long id);
	public List<Group> getGroupsByOwner(Person owner);
	public List<Group> getAllGroupsOfAMember(Person member);
	public void updateGroup(Group group);
	public List<Group> getAllPersonsGroups(Person person);
	void removeGroup(Group group);

	
}
