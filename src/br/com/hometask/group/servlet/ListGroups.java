package br.com.hometask.group.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.person.Person;

/**
 * Servlet implementation class ListGroups
 */
@WebServlet("/listGroups")
public class ListGroups extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private GroupController groupController;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListGroups() {
        super();
        this.groupController = new GroupController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		String json = "error";
		
		Person person = (Person) session.getAttribute("person");
			
		List<Group> groups = this.groupController.getAllGroupsOfAMember(person);

		if(groups != null){
			json = objectWriter.writeValueAsString(groups);
		}
		out.write(json);
	}

	/*
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

}
