package br.com.hometask.group.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.IGroupController;

/**
 * Servlet implementation class RemoveGroup
 */
@WebServlet("/removeGroup")
public class RemoveGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IGroupController groupController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveGroup() {
        super();
        
        groupController = new GroupController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		boolean html = false;
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try{
			long idGroup = Long.parseUnsignedLong(request.getParameter("idGroup"));
			
			Group group = groupController.getGroupById(idGroup);
			
			if(group != null){
				groupController.removeGroup(group);
				html = true;
			}
		}catch(RuntimeException e){
			
		}	
		String json = objectWriter.writeValueAsString(html);
		response.getWriter().write(json);
	}
}
