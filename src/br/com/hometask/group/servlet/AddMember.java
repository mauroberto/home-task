package br.com.hometask.group.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.IGroupController;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;

/**
 * Servlet implementation class addMember
 */
@WebServlet("/addMember")
public class AddMember extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private IPersonController personController;
    private IGroupController groupController;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddMember() {
        super();
        this.groupController = new GroupController();
        this.personController = new PersonController();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		String json = "error";
		
		Person person = (Person) request.getSession(false).getAttribute("person");
			
		String idGroup = request.getParameter("idGroup");
		String email = request.getParameter("email");
		
		if(email != null && idGroup != null){
			
			Person member = this.personController.getPerson(email);
			
			if(member != null){
				try{
					long idGroupLong = Long.parseLong(idGroup);
					
					Group group = this.groupController.getGroupById(idGroupLong);
					
					if(group != null){
						boolean isMember = false;
						for(Person groupMember : group.getMembers()){
							if(groupMember.getEmail().equals(member.getEmail())){
								isMember = true;
								break;
							}
						}
						
						if(!isMember){
							this.groupController.addMemberToGroup(group, member);
							json = objectWriter.writeValueAsString(member);
						}else{
							json = "isMember";
						}
					}
				}catch(RuntimeException e){
					
				}
			}else{
				json = "memberNotExists";
			}
		} else{
			json = "Invalid Parameters!";
		}
	
		out.write(json);
	}

}