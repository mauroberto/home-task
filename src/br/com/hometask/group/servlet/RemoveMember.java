package br.com.hometask.group.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.hometask.group.Group;
import br.com.hometask.group.GroupController;
import br.com.hometask.group.GroupPermissionException;
import br.com.hometask.group.IGroupController;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;

/**
 * Servlet implementation class RemoverMemberServlet
 */
@WebServlet("/removeMember")
public class RemoveMember extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	 IGroupController groupController;
	 IPersonController personController;
	 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveMember() {
        super();
        
        this.groupController = new GroupController();
        this.personController = new PersonController();
    }
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String returnMessage = "false-error";
		
		HttpSession session = request.getSession();
		
		String removedMemberID = request.getParameter("memberID");
		String groupID = request.getParameter("groupID");
		
		if(removedMemberID != null && groupID != null){
			
			Person remover = (Person) session.getAttribute("person");
			
			try{
				long memberRemovedId = Long.parseLong(removedMemberID);
				Person memberRemoved = this.personController.getOneById(memberRemovedId);
				Group group = this.groupController.getGroupById(Long.parseLong(groupID));
				
				if(this.groupController.removeMember(group, remover ,memberRemoved)){				
					returnMessage = "true";
					if(memberRemovedId == remover.getId()){
						returnMessage = "true-leaveGroup";
					}
				}else{
					returnMessage = "false-error";	
				}
				
			}catch (GroupPermissionException e) {
				e.printStackTrace();
				returnMessage = "false-permission";
			}catch(RuntimeException e){
				e.printStackTrace();
				returnMessage = "false-error";
			}
			
		}
		out.write(returnMessage);
	}
}
