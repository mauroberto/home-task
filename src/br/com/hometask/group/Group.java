package br.com.hometask.group;

import java.util.ArrayList;
import java.util.List;

import br.com.hometask.person.Person;

public class Group {

	long id;
	String name;
	long creator_id;
	List<Person> members;
	
	public Group() {
		members = new ArrayList<Person>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCreator_id() {
		return creator_id;
	}

	public void setCreator_id(long creator_id) {
		this.creator_id = creator_id;
	}

	public List<Person> getMembers() {
		return members;
	}

	public void setMembers(List<Person> members) {
		this.members = members;
	}
	
	public void addMember(Person person){
		this.members.add(person);
	}
	
	public void removeMember(Person person){
		this.members.remove(person);
	}
}
