package br.com.hometask.group;

import java.util.List;

import br.com.hometask.person.Person;

public interface IGroupMemberDAO {

	public void insert(Group group, Person newMember);
	public boolean remove(Group group, Person member);
	public List<Long> getAllByMember(Person member);
	public List<Long> getAllMembers(Group group);
	
}
