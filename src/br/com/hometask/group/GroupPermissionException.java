package br.com.hometask.group;

public class GroupPermissionException extends Exception {

	public GroupPermissionException(String message) {
		super(message);
	}
}
