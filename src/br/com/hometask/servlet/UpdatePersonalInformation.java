package br.com.hometask.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.hometask.DataValidator;
import br.com.hometask.HashSHA512;
import br.com.hometask.IDataValidator;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * Servlet implementation class UpdatePersonalInformation
 */
@WebServlet("/UpdatePersonalInformation")
public class UpdatePersonalInformation extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final String URL_UPDATE_INFORMATION = "admin/UpdatePersonalInformationForm.jsp";
	
	private IPersonController personController;
	
	private IDataValidator dataValidator;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdatePersonalInformation() {
        super();
        personController = new PersonController();
        dataValidator = new DataValidator();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException{
    	response.sendRedirect("admin/");
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{
			List<String> errors = new ArrayList<String>();	
			
		Person person = getRequestParams(request, errors);
		
		boolean valid = validatePersonInformation(person, errors);
		
		if(valid) personController.updatePerson(person);
		
		redirectToOriginForm(request, response, valid, person, errors);
		
		}catch(NumberFormatException e){
			response.sendRedirect(URL_UPDATE_INFORMATION);
		}
		
	}

	private Person getRequestParams(HttpServletRequest request, List<String> errors) {
		long id = Long.parseUnsignedLong(request.getParameter("id"));
		
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String passwordConfirm = request.getParameter("passwordConfirm");
		
		Person person = personController.getOneById(id);
		
		person.setId(id);
		
		person.setName(name);
		person.setEmail(email);
		
		if(password.equals(passwordConfirm)){
			password = new HashSHA512().get_SHA_512_SecurePassword(password);
			person.setPassword(password);
		}else{
			errors.add("A confirmação de senha deve ter o mesmo valor da senha.");
		}
			
		return person;
	}

	private boolean validatePersonInformation(Person person, List<String> errors) {
				
		if(!dataValidator.isEmailValid(person.getEmail())){
			errors.add("Email inválido.");
		}
		
		if(person.getName().trim().length() == 0){
			errors.add("O nome deve ser preenchido;");
		}
		
		if(person.getPassword().trim().length() < 6){
			errors.add("A senha deve ter no mínimo 6 caracteres");
		}
		
		return !(errors.size() > 0);
	}

	private void redirectToOriginForm(HttpServletRequest request,
			HttpServletResponse response, boolean modified, Person person, List<String> errors)
			throws ServletException, IOException {

		
		HttpSession session = request.getSession(false);
		
		int isModified = 0;
		if(modified){
			isModified = 1;
		}else{
			session.setAttribute("errors", errors);
		}
		
		session.setAttribute("modified", isModified);
		
		
		if(modified) session.setAttribute("person", person);
		
		response.sendRedirect(URL_UPDATE_INFORMATION);
	}

}
