package br.com.hometask.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import br.com.hometask.DataValidator;
import br.com.hometask.IDataValidator;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;
import br.com.hometask.person.verification.CadastreConfirmationController;
import br.com.hometask.person.verification.ICadastreConfirmationController;

@WebServlet(name="Add Photo", urlPatterns={"/admin/addPhoto"})

@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)   // 50MB

public class AddPhoto extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String SAVE_DIR = "admin"+File.separator+"user_images";
	
	IPersonController personController;
	IDataValidator dataValidator;
	ICadastreConfirmationController cadastreConfirmationController;
       
    public AddPhoto() {
        super();
        // TODO Auto-generated constructor stub
        this.personController = new PersonController();
        this.dataValidator = new DataValidator();
        this.cadastreConfirmationController = new CadastreConfirmationController();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("");
	}
	
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
	        
	        if (isMultipart) {
	        	Person person = (Person)request.getSession().getAttribute("person");
	        	// Create a factory for disk-based file items
	        	FileItemFactory factory = new DiskFileItemFactory();

	        	// Create a new file upload handler
	        	ServletFileUpload upload = new ServletFileUpload(factory);
	 
	            try {
	            	// Parse the request
	            	List /* FileItem */ items = upload.parseRequest(request);
	                Iterator iterator = items.iterator();
	                while (iterator.hasNext()) {
	                    FileItem item = (FileItem) iterator.next();
	                    if (!item.isFormField()) {
	                        String fileName = item.getName();
	                        fileName = new Random().nextInt(999999999)+fileName;
	                        String root = getServletContext().getRealPath("/");
	                        File path = new File(root + File.separator + SAVE_DIR);
	                        if (!path.exists()) {
	                            boolean status = path.mkdirs();
	                        }
	                        String pathFinal = path+File.separator + fileName;
	                        File uploadedFile = new File(pathFinal);
	                        System.out.println(uploadedFile.getAbsolutePath());
	                        item.write(uploadedFile);
	                        
	                        person.setId(person.getId());
	                        person.setProfilePicturePATH("user_images" + File.separator + fileName);
	                        personController.updateImage(person);
	                        request.getSession().setAttribute("person", person);
	                    }
	                }
	            } catch (FileUploadException e) {
	            	System.out.println(e.getMessage());
	                e.printStackTrace();
	            } catch (Exception e) {
	            	System.out.println(e.getMessage());
	                e.printStackTrace();
	            }
	            response.sendRedirect("");
	        }
	    }
}
