package br.com.hometask.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import br.com.hometask.HashSHA512;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;

/**
 * Servlet implementation class DeactivateAccount
 */
@WebServlet(name="deactivate account", urlPatterns={"/deactivate"})
public class DeactivateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	IPersonController personController;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeactivateAccount() {
        super();
        this.personController = new PersonController();
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		boolean html = false;
		HttpSession session = request.getSession(false);
		String password = request.getParameter("password");
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		Person person = (Person) session.getAttribute("person");
		
		if(password != null){
			password = new HashSHA512().get_SHA_512_SecurePassword(password);

			if(person.getPassword().equals(password)){
				person.setActive(false);
				personController.updatePerson(person);
				html = true;
				session.invalidate();
			}else{
				html = false;
			}
		}else{
			html = false;
		}
		System.out.println(html);
		String json = objectWriter.writeValueAsString(html);
		out.write(json);
	}
}
