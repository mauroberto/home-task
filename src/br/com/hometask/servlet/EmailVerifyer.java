package br.com.hometask.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.hometask.person.IPersonController;
import br.com.hometask.person.PersonController;

/**
 * Servlet implementation class EmailVerifyer
 */
@WebServlet(name="Email Verifyer", urlPatterns={"/emailVerifyer"})
public class EmailVerifyer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	IPersonController personController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmailVerifyer() {
        super();
        this.personController = new PersonController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String html = "";
		
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String email = request.getParameter("email");
		
		if(email != null && !personController.doesEmailAlreadyExists(email)){
			html = "true";
		} else{
			html = "false";
		}
		
		out.write(html);
	}

}
