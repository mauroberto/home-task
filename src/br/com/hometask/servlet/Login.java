package br.com.hometask.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.hometask.HashSHA512;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;

/**
 * Servlet implementation class Login
 */
@WebServlet(name="login", urlPatterns={"/login"})
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IPersonController personController;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        this.personController  = new PersonController();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		if(email != null && password != null){
			password = new HashSHA512().get_SHA_512_SecurePassword(password);
			Person person = this.personController.getPerson(email);
			
			if(person != null && person.getPassword().equals(password) && person.isActive()){
				
				HttpSession session = request.getSession();
				
				session.setAttribute("person", person);
				out.print("true");
			}else if(person != null && !person.isActive()){
				out.print("desactived");
			}else{
				out.print("fail");
			}
		}else{
			out.print("fail");
		}
	}

}
