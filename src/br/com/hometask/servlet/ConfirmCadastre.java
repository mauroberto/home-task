package br.com.hometask.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.hometask.person.verification.CadastreAlreadyActive;
import br.com.hometask.person.verification.CadastreConfirmationController;
import br.com.hometask.person.verification.ICadastreConfirmationController;
import br.com.hometask.person.verification.IVerificationTokenDAO;
import br.com.hometask.person.verification.VerificationToken;
import br.com.hometask.person.verification.exception.InvalidTokenException;

/**
 * Servlet implementation class ConfirmRegistration
 */
@WebServlet("/ConfirmCadastre")
public class ConfirmCadastre extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ICadastreConfirmationController cadastreConfirmationController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmCadastre() {
        super();
        cadastreConfirmationController = new CadastreConfirmationController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String token = request.getParameter("token");
		
		VerificationToken verificationToken = new VerificationToken();
		
		verificationToken.setToken(token);
		
		try {
			
			cadastreConfirmationController.confirmCadastre(verificationToken);
			
			response.sendRedirect("admin/");
			
		} catch (InvalidTokenException e) {
			
			// TODO Redirect to error message
			e.printStackTrace();
		} catch (CadastreAlreadyActive e) {
			
			response.sendRedirect("admin/");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

}
