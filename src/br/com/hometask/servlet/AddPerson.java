package br.com.hometask.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.hometask.DataValidator;
import br.com.hometask.HashSHA512;
import br.com.hometask.IDataValidator;
import br.com.hometask.person.IPersonController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;
import br.com.hometask.person.verification.CadastreAlreadyActive;
import br.com.hometask.person.verification.CadastreConfirmationController;
import br.com.hometask.person.verification.ICadastreConfirmationController;

/**
 * Servlet implementation class AddPerson
 */
@WebServlet(name="Add Person", urlPatterns={"/addPerson"})
public class AddPerson extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	IPersonController personController;
	IDataValidator dataValidator;
	ICadastreConfirmationController cadastreConfirmationController;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPerson() {
        super();
        // TODO Auto-generated constructor stub
        this.personController = new PersonController();
        this.dataValidator = new DataValidator();
        this.cadastreConfirmationController = new CadastreConfirmationController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("admin/");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("tentando cadastrar ");
		
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		request.setAttribute("name",name);
		request.setAttribute("email",email);
		
		String error = null;
		
		
		if(email != null && !this.personController.doesEmailAlreadyExists(email)){
			
			if(name != null &&
				password != null){
				
				
				Person person = new Person();
				person.setName(name.trim());
				person.setEmail(email);
				password = new HashSHA512().get_SHA_512_SecurePassword(password);
				person.setPassword(password);
				person.setActive(false);
				person.setProfilePicturePATH("user_images/default.png");
				
				if(this.dataValidator.isPersonInformationValid(person)){
					
					this.personController.addPerson(person);
					
					try {
						
						this.cadastreConfirmationController.createVerficationToken(person);
						
					} catch (CadastreAlreadyActive e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
					
						response.sendRedirect("admin/");
					}
					return;
				}else{
					error = "Informações inválidas";
				}
			} else {
				error = "Email já existe";
				
			}
		}else{
			error = "Email já existe";
			
			
		}
		
		request.setAttribute("error", error);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

}
