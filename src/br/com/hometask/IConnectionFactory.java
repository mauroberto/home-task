package br.com.hometask;

import java.sql.Connection;

public interface IConnectionFactory {
	public Connection getConnection();
}
