package br.com.hometask.person;

public class Person{
	
	private long id;
	private String name;
	private String email;
	private String password;
	private String profilePicturePATH;
	private boolean active;
	
	
	public String getProfilePicturePATH(){
		return this.profilePicturePATH;
	}
	public void setProfilePicturePATH(String path){
		this.profilePicturePATH = path;
	}
	public boolean isActive(){
		return this.active;
	}
	public void setActive(boolean value){
		this.active = value;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public boolean equals(Object object){
		
		if(object instanceof Person && this.id == ((Person) object).id){
			return true;
		}
		
		return false;
	}
}
