package br.com.hometask.person;

public interface IPersonDAO {
	
	public Person insert(Person person);
	
	public void update(Person person);
	
	public void updateImage(Person person);
	
	public void updatePassword(Person person);
	
	public Person getOne(long id);
	
	public Person getAll(long id);
	
	public Person getByEmail(String email);
	public Person getByID(long id);

}
