package br.com.hometask.person;


public class PersonController implements IPersonController{

	IPersonDAO personDAO;
	
	
	public PersonController() {
		this.personDAO = new PersonDAO();
	}
	
	public void addPerson(Person person){
		this.personDAO.insert(person);
	}
	
	public boolean doesEmailAlreadyExists(String email){
		if(this.personDAO.getByEmail(email) != null){
			return true;
		}
		return false;
	}
	
	public Person getPerson(String email){
		return this.personDAO.getByEmail(email);
	}

	@Override
	public void updatePerson(Person person) {
		this.personDAO.update(person);
	}
	
	@Override
	public void updateImage(Person person) {
		this.personDAO.updateImage(person);
	}

	@Override
	public Person getOneById(long id) {
		return personDAO.getOne(id);
	}

	@Override
	public void activePerson(Person person) {
		
		person.setActive(true);
		
		personDAO.update(person);
		
	}

	@Override
	public void updatePassword(Person person) {
		personDAO.updatePassword(person);
	}

	
}
