package br.com.hometask.person.verification;

public interface IVerificationTokenDAO {
	public void insert(VerificationToken token);
	
	public VerificationToken getOne(String token);

	public void delete(VerificationToken token);
	
	public void update(VerificationToken token);
}
