package br.com.hometask.person.verification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import br.com.hometask.ConnectionFactory;
import br.com.hometask.person.PersonDAO;

public class VerificationTokenDAO implements IVerificationTokenDAO{

	private ConnectionFactory factory;
	
	public VerificationTokenDAO() {
		factory = new ConnectionFactory();
	}

	@Override
	public void insert(VerificationToken token) {
		
		Connection connection = factory.getConnection();
		
		String sql = "insert into verification_token (token, person_id,creation_date,valid) values (?,?,?,?)";
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, token.getToken());
			statement.setLong(2,token.getPerson().getId());
			statement.setLong(3, token.getCreationDate().getTime());
			statement.setBoolean(4, true);
			
			statement.execute();
			
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public VerificationToken getOne(String token) {
		
		Connection connection = factory.getConnection();
		
		VerificationToken verficationToken = null;
		
		String sql = "select token,person_id,creation_date,valid from verification_token where token = ?";
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, token);
			
			ResultSet result = statement.executeQuery();
			
			if(result.next()){
				
				PersonDAO personDao = new PersonDAO();
				
				verficationToken = new VerificationToken();
				
				verficationToken.setToken(result.getString(1));
				
				verficationToken.setPerson(personDao.getByID(result.getLong(2)));
				
				verficationToken.setCreationDate(new Date(result.getLong(3)));
				
				verficationToken.setValid(result.getBoolean(4));
			}
			
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return verficationToken;
	}

	@Override
	public void delete(VerificationToken token) {
		
		String sql = "delete from verification_token where token = ?";
		
		Connection connection = factory.getConnection();
		
		try {
			
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setString(1, token.getToken());
			
			statement.executeUpdate();
			
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(VerificationToken token) {
		String sql = "update verification_token set valid = ?";
		
		Connection connection = factory.getConnection();
		
		try {
			
			PreparedStatement statement = connection.prepareStatement(sql);
			
			statement.setBoolean(1, token.isValid());
			
			statement.executeUpdate();
			
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
}
