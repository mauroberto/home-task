package br.com.hometask.person.verification;

import java.util.Date;

import br.com.hometask.person.Person;

public class VerificationToken {

	private String token;
	
	private Person person;
	
	private Date creationDate;
	
	private boolean valid;
	
	public void setValid(boolean valid){
		this.valid = valid;
	}
	
	public boolean isValid(){
		return valid;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
}
