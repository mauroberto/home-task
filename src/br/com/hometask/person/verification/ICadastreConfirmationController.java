package br.com.hometask.person.verification;

import br.com.hometask.person.Person;
import br.com.hometask.person.verification.exception.InvalidTokenException;

public interface ICadastreConfirmationController {
	
	public void createVerficationToken(Person person) throws CadastreAlreadyActive;
	
	public void confirmCadastre(VerificationToken token) throws InvalidTokenException, CadastreAlreadyActive;

	public void desactiveVerificationToken(VerificationToken token);
	
	public void removerVerificationToken(VerificationToken token);

}
