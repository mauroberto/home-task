package br.com.hometask.person.verification;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;

import br.com.hometask.email.Email;
import br.com.hometask.email.EmailController;
import br.com.hometask.person.Person;
import br.com.hometask.person.PersonController;
import br.com.hometask.person.verification.exception.InvalidTokenException;

public class CadastreConfirmationController implements ICadastreConfirmationController{

	private IVerificationTokenDAO verificationTokenDAO;
	
	private EmailController emailController;
	
	private PersonController personController;
	
	
	public CadastreConfirmationController() {
		
		verificationTokenDAO = new VerificationTokenDAO();
		
		emailController = new EmailController();
		
		personController = new PersonController();

	}

	@Override
	public void createVerficationToken(Person person) throws CadastreAlreadyActive {
		
			if(!person.isActive()){
				Random random = new Random();
				
				String data = person.getId()+person.getEmail()+random.nextDouble();
				
				String token = DigestUtils.sha512Hex(data);
				
				VerificationToken verificationToken = new VerificationToken();
				
				verificationToken.setToken(token);
				
				verificationToken.setPerson(person);
				
				verificationToken.setCreationDate(new Date());
				
				verificationToken.setValid(true);
				
				verificationTokenDAO.insert(verificationToken);
				
				//Send email
				sendEmailForConfirmation(person, verificationToken);
				
				//Job to delete token after 24 hours
				createJobToDeleteVerificationToken(token, verificationToken);
			}else{
				throw new CadastreAlreadyActive();
			}
	}

	private void createJobToDeleteVerificationToken(String token,
			VerificationToken verificationToken) {
		JobDataMap jobData = new JobDataMap();
		
		jobData.put("token", verificationToken);
		
		JobDetail job = newJob(DeleteVerificationTokenJob.class)
				.withIdentity(token, "verification_token").usingJobData(jobData).build();
		
		Date start = new Date(verificationToken.getCreationDate().getTime()+86400000);
		
		SimpleTrigger trigger = (SimpleTrigger) newTrigger().withIdentity(token)
				.startAt(start).forJob(job).build();
		
		Scheduler scheduer;
		try {
			scheduer = new StdSchedulerFactory().getScheduler();
			
			scheduer.start();
			
			scheduer.scheduleJob(job, trigger);
			
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void sendEmailForConfirmation(Person person,
			VerificationToken verificationToken) {
		
		Email email = new Email();
		
		email.setSender("italosestilon@gmail.com");
		
		email.setRecipient(person.getEmail());
		
		email.setSubject("Confirme o cadastro");
		
		email.setText("http://localhost:8080/HomeTask2/ConfirmCadastre?token="+verificationToken.getToken());
		
		emailController.send(email);
	}

	@Override
	public void confirmCadastre(VerificationToken verificationToken) throws InvalidTokenException, CadastreAlreadyActive{
		
		VerificationToken token = verificationTokenDAO.getOne(verificationToken.getToken());
		
		if(token != null){
			
			if(token.isValid()){
			
				personController.activePerson(token.getPerson());
				
				desactiveVerificationToken(token);
			}else{
				
				throw (new CadastreAlreadyActive());
			}
			
		}else{
			
			throw (new InvalidTokenException());
		}
		
	}

	@Override
	public void desactiveVerificationToken(VerificationToken token) {
		
		token.setValid(false);
		verificationTokenDAO.update(token);
		
	}

	@Override
	public void removerVerificationToken(VerificationToken token) {
		verificationTokenDAO.delete(token);
		
	}
	
}
