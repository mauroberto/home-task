package br.com.hometask;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.hometask.group.Group;
import br.com.hometask.person.Person;
import br.com.hometask.task.Task;

public class DataValidator implements IDataValidator{

	@Override
	public boolean isEmailValid(String email) {
		
		if(email == null)
			return false;
		
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		Pattern pattern = java.util.regex.Pattern.compile(ePattern);
		Matcher matcher = pattern.matcher(email);
		
        return matcher.matches();
	}

	@Override
	public boolean isPersonInformationValid(Person person) {
		
		boolean flag = true;
		String name = person.getName().trim();
		name = this.removeHTML(name);
		person.setName(name);
		
		if(!this.isEmailValid(person.getEmail())){
			flag = false;
		}
		
		if(!this.isPersonNameValid(person.getName())){
			flag = false;
		}
		
		if(person.getPassword().length() < 6){
			flag = false;
		}
		
		
		return flag;
	}
	
	@Override
	public boolean isPersonNameValid(String name){
		
		if(name == null || name.length() > 50)
			return false;
		
		name = removeHTML(name);
		String ePattern = "^[\\sa-zA-Zà-úÀ-ÚÇç.!#$%&'*+/=?^_`{|}~-]+";
		Pattern pattern = java.util.regex.Pattern.compile(ePattern);
		Matcher matcher = pattern.matcher(name);
		
		return matcher.matches();
	}

	@Override
	public boolean isGroupInformationValid(Group group) {
		
		if(group.getName() == null)
			return false;
		
		String name = group.getName().trim();
		name = this.removeHTML(name);
		group.setName(name);
		
		if(group.getName().length() <= 0)
			return false;
		
		
		
		return true;
	}

	@Override
	public boolean isTaskInformationValid(Task task) {
		
		String name = task.getName().trim();
		String description = task.getDescription().trim();
		
		name = this.removeHTML(name);
		description = this.removeHTML(description);
		
		task.setName(name);
		task.setDescription(description);
		
		if(task.getName() == null || task.getDescription() == null || task.getDate() == null)
			return false;
		
		if(task.getName().length() <= 0){
			return false;
		}
		
		if(task.getDescription().length() <= 0)
			return false;
		
		return true;
	}
	
	
	private String removeHTML(String string){
		string = string.replace(">", "&gt;");
		return string.replaceAll("<", "&lt;");
	}
}
